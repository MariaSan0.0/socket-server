const express = require('express');
const path = require('path')
require('dotenv').config();
//app de Express
const app = express();

//crear servidor de node
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sokets/socket');


const {PORT} = process.env

//path publico
const pulicPath = path.resolve(__dirname, 'public');
app.use(express.static(pulicPath));

server.listen(PORT, (err) => {
    if (err) throw new Error(err);
    console.log('servidor corriendo.', PORT);
});



